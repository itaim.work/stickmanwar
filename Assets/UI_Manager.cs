using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    [SerializeField] private GameObject PauseMenu, LoseMenu, WinMenu, GameplayButtons;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            PauseGame();
        }
    }

    public void Resume()
    {
        GameplayButtons.SetActive(true);

        Time.timeScale = 1;
        PauseMenu.SetActive(false);
    }
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
        GameplayButtons.SetActive(false);
        PauseMenu.SetActive(true);
    }

    public void LoseGame()
    {
        Time.timeScale = 0;
        LoseMenu.SetActive(true);
    }
    public void WinGame()
    {
        Time.timeScale = 0;
        WinMenu.SetActive(true);
    }
}
