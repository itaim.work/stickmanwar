using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class WarriorController : MonoBehaviour
{

    [SerializeField] private Transform EnemyBase;
    [SerializeField] private Transform target;
    [SerializeField] private NavMeshAgent WarriorAgent;
    [SerializeField] private bool IsRed;
    [SerializeField] private float counter, CounterReset;
    [SerializeField] private bool AttackMode = false;
    [SerializeField] private bool AtEnemyBase;
    [SerializeField] private HealthBar HealthBar;
    [SerializeField] private int HP;
    [SerializeField] private int Damage;
    [SerializeField] private BoardManager BoardManagerScript;



    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
        WarriorAgent = gameObject.GetComponent<NavMeshAgent>();
        BoardManagerScript = GameObject.Find("BoardManager").GetComponent<BoardManager>();

        if (!IsRed)
        {
            EnemyBase = GameObject.Find("Player_Base").transform;
        }
        else
        {
            EnemyBase = GameObject.Find("AI_Base").transform;
        }
        EnemyBase.GetComponent<Base_Controller>().AddWarrior(this);
        target = EnemyBase;

        HealthBar.SetMaxHealth(HP);
        WarriorAgent.SetDestination(target.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (!AtEnemyBase)
        {
            if (target == null)
            {
                SetTarget(EnemyBase.gameObject);
                AttackMode = false;
                WarriorAgent.SetDestination(target.position);
            }

            if (Vector3.Distance(target.position, transform.position) <= 5f)
            {
                WarriorAgent.SetDestination(target.position);
                if (WarriorAgent.remainingDistance <= 1f)
                {
                    WarriorAgent.SetDestination(transform.position);
                    GetComponent<Rigidbody>().velocity = Vector3.zero;
                    GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                    AttackMode = true;
                }
            }
            if (AttackMode && counter <= 0)
            {
                if (target.CompareTag("Warrior"))
                {
                    target.gameObject.GetComponent<WarriorController>().TakeDamage(Damage);
                }
                counter = CounterReset;
            }


            if (HP <= 0)
            {
                if (IsRed)
                {
                    BoardManagerScript.SetRedSoldierCounter(-1);
                }
                else
                {
                    BoardManagerScript.SetBlueSoldierCounter(-1);
                }


                Destroy(gameObject);
            }
            counter -= Time.deltaTime;

        }
        else if (WarriorAgent.remainingDistance <= 1f)
        {
            WarriorAgent.SetDestination(transform.position);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            if (counter <= 0)
            {
                target.gameObject.GetComponent<Base_Controller>().TakeDamage(Damage);
                counter = CounterReset;
            }
            if (HP <= 0)
            {
                EnemyBase.GetComponent<Base_Controller>().RemoveWarrior(this);
                Destroy(gameObject);
            }
            counter -= Time.deltaTime;

        }

    }

    public void SetTarget(GameObject NewTarget)
    {
        this.target = NewTarget.transform;
    }

    public void TakeDamage(int damage)
    {
        HP -= damage;
        HealthBar.SetHealth(HP);
    }

    public void arrivedAtBase()
    {
        AtEnemyBase = true;
    }

    public bool GetIsRed()
    {
        return IsRed;
    }

    public bool GetAttackMode()
    {
        return this.AttackMode;
    }

    public bool GetAtEnemyBase()
    {
        return AtEnemyBase;
    }

    public NavMeshAgent getAgent()
    {
        return this.WarriorAgent;
    }
}
