using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base_Controller : MonoBehaviour
{


    [SerializeField] private Transform WorkerSpawn;
    [SerializeField] private GameObject WorkerPrefab;

    [SerializeField] private Transform SoldierSpawn;
    [SerializeField] private GameObject SoldierPrefab;

    [SerializeField] private HealthBar HealthBar;

    [SerializeField] private Text MoneyText;

    [SerializeField] private int HP, money;
    [SerializeField] private float Counter, CounterReset;

    [SerializeField] private GameObject WorkerButton;
    [SerializeField] private GameObject SoldierButton;
    [SerializeField] private bool IsRed, Hold;

    [SerializeField] private BoardManager BoardManagerScript;
    [SerializeField] private List<WarriorController> Warriors;
    [SerializeField] private UI_Manager UI_Manager_Script;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;

        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();

        HealthBar.SetMaxHealth(HP);

        BoardManagerScript = GameObject.Find("BoardManager").GetComponent<BoardManager>();

        MoneyText = GameObject.Find("MoneyDisplayText").GetComponent<Text>();
        WorkerButton = GameObject.Find("WorkerSpawnButton");

        SoldierButton = GameObject.Find("SoldierSpawnButton");
        CounterReset = 10;
        Counter = CounterReset;

        Warriors = new List<WarriorController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (    IsRed)
        {
            if (money < 250)
            {
                SoldierButton.GetComponent<Button>().enabled = false;
                SoldierButton.GetComponentInChildren<Text>().text = "Insufficient funds.";

            }
            else
            {
                SoldierButton.GetComponent<Button>().enabled = true;
                SoldierButton.GetComponentInChildren<Text>().text = "Soldier - 250$";
            }

            if (money < 100)
            {
                WorkerButton.GetComponent<Button>().enabled = false;
                WorkerButton.GetComponentInChildren<Text>().text = "Insufficient funds.";
            }
            else
            {
                WorkerButton.GetComponent<Button>().enabled = true;
                WorkerButton.GetComponentInChildren<Text>().text = "Worker - 100$";
            }
            MoneyText.text = "Money Amount: " + money;

        }

        if (Counter <= 0)
        {
            money += 100;
            Counter = CounterReset;
        }
        Counter -= Time.deltaTime;

        if (HP <= 0)
        {
            if (IsRed)
            {
                UI_Manager_Script.LoseGame();
            }
            else
            {
                UI_Manager_Script.WinGame();

            }
        }
    }

    public void SpawnWorker()
    {
        Instantiate(WorkerPrefab, WorkerSpawn.position, Quaternion.identity);
        money -= 100;
    }

    public void Stop()
    {
        foreach (WarriorController warrior in Warriors)
        {
            //warrior.getAgent().speed = 0;
        }
    }

    public void Attack()
    {
        foreach (WarriorController warrior in Warriors)
        {
            // warrior.getAgent().speed = 5;
        }

    }

    public void SpawnSoldier()
    {
        Instantiate(SoldierPrefab, SoldierSpawn.position, Quaternion.identity);
        if (IsRed)
        {
            BoardManagerScript.SetRedSoldierCounter(1);
        }
        else
        {
            BoardManagerScript.SetBlueSoldierCounter(1);
        }
        money -= 250;

    }

    public void TakeDamage(int Damage)
    {
        HP -= Damage;
        HealthBar.SetHealth(HP);

    }

    public int GetMoney()
    {
        return money;
    }

    public void SetMoney(int money)
    {
        this.money = money;
    }

    public bool GetIsRed()
    {
        return IsRed;
    }

    public void AddWarrior(WarriorController warrior)
    {
        Warriors.Add(warrior);
    }
    public void RemoveWarrior(WarriorController warrior)
    {
        Warriors.Remove(warrior);
    }
}
