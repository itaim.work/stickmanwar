using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior_TriggerController : MonoBehaviour
{
    [SerializeField] private WarriorController ParentWarriorController;
    [SerializeField] private bool IsRed;

    private void Start()
    {
        ParentWarriorController = gameObject.GetComponentInParent<WarriorController>();
        IsRed = ParentWarriorController.GetIsRed();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Warrior") && !gameObject.GetComponentInParent<WarriorController>().GetAtEnemyBase())
        {
            if (IsRed)
            {
                if (!other.GetComponent<WarriorController>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().SetTarget(other.gameObject);
                }
            }
            else
            {
                if (other.GetComponent<WarriorController>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().SetTarget(other.gameObject);

                }
            }
        }
        else if (other.transform.CompareTag("Base") && !gameObject.GetComponentInParent<WarriorController>().GetAtEnemyBase()) 
        {


            if (IsRed)
            {
                if (!other.GetComponent<Base_Controller>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().arrivedAtBase();
                }
            }
            else
            {
                if (other.GetComponent<Base_Controller>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().arrivedAtBase();
                }

            }



        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (!gameObject.GetComponentInParent<WarriorController>().GetAttackMode() && other.GetComponent<WarriorController>() != null && !gameObject.GetComponentInParent<WarriorController>().GetAtEnemyBase())
        {
            if (IsRed)
            {
                if (!other.GetComponent<WarriorController>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().SetTarget(other.gameObject);
                }
            }
            else
            {
                if (other.GetComponent<WarriorController>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().SetTarget(other.gameObject);

                }
            }
        }
        else if (other.transform.CompareTag("Base") && !gameObject.GetComponentInParent<WarriorController>().GetAtEnemyBase() && other.GetComponent<Base_Controller>() != null)
        {

            if (IsRed)
            {
                if (!other.GetComponent<Base_Controller>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().arrivedAtBase();
                }
            }
            else
            {
                if (other.GetComponent<Base_Controller>().GetIsRed())
                {
                    gameObject.GetComponentInParent<WarriorController>().arrivedAtBase();
                }

            }
        }

    }
}
