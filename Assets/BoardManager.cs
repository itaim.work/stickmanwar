using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{

    [SerializeField] private int RedSoldierCounter, BlueSoldierCounter;

    public void SetRedSoldierCounter( int value)
    {
        RedSoldierCounter += value;
    }
    public void SetBlueSoldierCounter(int value)
    {
        BlueSoldierCounter+=value;
    }
    public int GetRedSoldierCounter()
    {
        return RedSoldierCounter;
    }
    public int GetBlueSoldierCounter()
    {
        return BlueSoldierCounter;
    }

}
